# 0.1.2 Shared components (wip)

* Shared components between entities
* Shared components example

# 0.1.1 Initial release

* Register entities with components using builder
* Register systems with priority using builder
* Register entity container
* Run systems and read / write entity components
* Basic example